<?php
if (isset($_FILES['image']['name'])) {
    try {
        $present = $_POST['isPublic'] == 'on' ? true : false;
        $requete = "INSERT INTO article (id, titre, slug, resume, content, isPublic, image) "
                . "VALUES (NULL, '" . $_POST['titre'] . "', '" . $_POST['slug'] . "', '" . $_POST['resume'] . "', '" . $_POST['content'] . "', '" . $present . "', '" . $_FILES['image']['name'] . "')";
        $bdd = new PDO('mysql:host=localhost;port=3306;dbname=blog', "root", "");
        $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $statement = $bdd->query($requete);
        var_dump($statement);
        $bdd = null;
    } catch (Exception $e) {
        echo "<pre>";
        var_dump($e);
        echo "</pre>";
    }
}
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" href="asset/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
    </head>
    <body>
        <nav class="navbar-expand-lg navbar navbar-dark bg-primary">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="./">Accueil </a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="./creerArticle.php">Rédiger un nouvel article</a>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="container-fluid">
            <h1>Créer un article</h1>
            <form enctype="multipart/form-data"  class="col-md-8 offset-md-2" action="" method="POST">
                <div class="form-group">
                    <label for="titre">Titre</label>
                    <input name="titre" type="text" class="form-control" id="titre" aria-describedby="titreBlog" placeholder="Saisir le titre de l'article">
                    <small id="titreBlog" class="form-text text-muted">Attention le titre d'un article représente son contenu. Vous pourrez toujours le modifier plus tard</small>
                </div>
                <div class="form-group">
                    <label for="slug">Slug</label>
                    <input name="slug" type="text" class="form-control" id="slug" aria-describedby="slugBlog" placeholder="Saisir un slug">
                    <small id="slugHelp" class="form-text text-muted">Un slug est le lien de l'article</small>
                </div>
                <div class="form-group">
                    <label for="contentBlog">Résumé</label>
                    <textarea name="resume" class="form-control" id="contentBlog" rows="3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin vitae turpis varius, finibus elit id, consequat urna. Sed rhoncus metus non lorem porttitor, ut luctus urna aliquet. Duis lacus lorem, tincidunt eu ante quis, scelerisque egestas augue nullam.</textarea>
                </div>
                <div class="form-group">
                    <label for="contentBlog">Contenu</label>
                    <textarea name="content" class="form-control" id="contentBlog" rows="10">
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at velit id felis sodales finibus a a lacus. Vivamus id metus ac massa semper gravida. Etiam ac orci in tortor ullamcorper lobortis quis a nulla. Ut aliquam, magna sit amet pellentesque vulputate, enim lorem lobortis ante, et ullamcorper mi mauris in risus. Donec non orci sed nibh auctor semper. Morbi vulputate facilisis ligula vel finibus. Suspendisse volutpat lacus magna, sed varius odio fermentum at. In varius nisl at risus dignissim convallis. Maecenas quis ultrices dolor, a vulputate leo. Fusce porttitor vestibulum hendrerit. Pellentesque nec risus dignissim, ullamcorper est sit amet, tincidunt magna. Nunc porttitor nunc eget feugiat ultricies.

Curabitur sodales suscipit turpis quis euismod. Duis at varius est. Aliquam elementum volutpat lectus quis sollicitudin. In in cursus dui. Phasellus tincidunt dictum vestibulum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nunc ut dolor suscipit metus accumsan auctor.

Suspendisse cursus dapibus nisi ut consectetur. Etiam efficitur sapien tristique velit consequat, nec semper lorem pretium. Phasellus congue metus mattis elit ullamcorper, ac pharetra risus tincidunt. Sed id elit varius, ornare justo feugiat, molestie lectus. Aenean tempor, ipsum eget vulputate volutpat, mauris augue sodales diam, id bibendum dolor nulla non orci. Pellentesque ultricies, metus ac convallis aliquet, ipsum elit feugiat lectus, et vehicula magna nunc in risus. Nunc metus purus, porta id odio vitae, condimentum consequat nisl. Nullam rhoncus dui quis ligula placerat egestas. Suspendisse ultricies id quam et lobortis. Quisque felis eros, ullamcorper quis fringilla id, commodo nec quam.

Aliquam ac tempus ex, vitae iaculis nulla. Sed sagittis nunc tellus, sit amet euismod dui placerat in. Phasellus erat elit, molestie id leo eget, malesuada sodales dolor. Proin sit amet quam blandit, laoreet sapien eu, feugiat metus. Mauris nec purus sit amet nulla porttitor ultrices. Suspendisse pharetra ultricies nibh vel congue. Praesent condimentum, elit eget malesuada luctus, libero leo suscipit sem, et venenatis libero risus quis eros.

Sed dapibus augue ex, sit amet vestibulum ipsum pretium sed. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. In et nibh nec ligula suscipit mollis. Donec non vehicula nulla, non rhoncus mauris. Sed in consequat tortor. Quisque facilisis vestibulum elementum. Sed porttitor elementum elit, et pretium felis mollis et.

Nulla condimentum et ex et hendrerit. Donec auctor mi sed feugiat bibendum. Vestibulum interdum quis arcu non tincidunt. Nullam pulvinar sapien tristique egestas eleifend. Aenean nisi ipsum, iaculis nec sapien quis, accumsan porttitor turpis. Vestibulum ornare sollicitudin sem id ornare. Sed efficitur nulla pretium orci tincidunt sagittis. In at tincidunt ligula. Quisque lobortis metus ac nunc scelerisque sodales. Praesent nec rutrum nisl.

Vestibulum auctor risus non congue lacinia. Nullam non mauris ex. Vestibulum eu rutrum dolor, tincidunt blandit turpis. Vestibulum ligula ligula, tempus nec tempus sit amet, fermentum ac nunc. Quisque porttitor lorem sed augue convallis, vitae placerat lacus commodo. Maecenas quis volutpat arcu. Aliquam convallis orci leo, at congue sapien porta sed. Etiam fringilla, tortor eu porta rutrum, turpis dolor maximus eros, quis efficitur augue augue quis turpis. Mauris pulvinar ac orci vel interdum. Sed at est non risus elementum faucibus.

Aliquam at vestibulum augue. Aenean molestie tincidunt placerat. Duis tincidunt suscipit pretium. Phasellus fermentum nunc mauris, vel facilisis justo pretium nec. In hac habitasse platea dictumst. Cras quis magna vehicula dolor malesuada lacinia at ut arcu. Pellentesque maximus dolor nec nulla imperdiet fermentum. Nunc mollis efficitur est, non molestie justo cursus luctus. Quisque dignissim, arcu non dictum efficitur, turpis lacus posuere diam, sit amet eleifend libero turpis venenatis risus. Maecenas quis tempor magna. Pellentesque non risus sagittis enim ultrices ullamcorper. Praesent maximus lectus sit amet erat vestibulum molestie.

Sed pulvinar viverra nunc, ac efficitur ex fringilla quis. Suspendisse nec sem sit amet turpis condimentum sollicitudin. Duis ut tristique ex. Quisque sapien neque, suscipit sit amet dictum elementum, bibendum et lorem. Aliquam ut felis non turpis aliquet elementum ut id lorem. Nunc ut consequat felis. Quisque id accumsan est, eget varius arcu. Aliquam congue dolor ultricies ultrices sodales. Vestibulum quis malesuada dolor, a accumsan nisi. Nunc arcu felis, rhoncus eget dolor a, posuere ullamcorper mauris.

Morbi vulputate ante ac orci lacinia laoreet. Donec ante nulla, cursus at ipsum sit amet, mattis tempor sapien. Vestibulum quis lectus massa. Proin finibus rutrum dignissim. Phasellus pharetra lacinia leo non ullamcorper. Quisque feugiat egestas sem, eu molestie tellus bibendum id. Nulla eros erat, facilisis eu purus sit amet, malesuada pharetra elit. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam pellentesque magna a risus ultricies viverra. Duis pharetra finibus elit, eget vehicula purus molestie et. Etiam sed dapibus nulla. Pellentesque eget sem nec quam iaculis laoreet non lacinia sapien. In porttitor, nulla et sagittis lobortis, metus mi vehicula tellus, non molestie justo mauris quis leo. Sed nec luctus sem, interdum ultrices orci.

Vestibulum feugiat eget turpis ut laoreet. Sed neque elit, auctor at elit quis, aliquam faucibus tortor. Nulla orci diam, posuere vitae viverra ac, pretium eu nisl. Interdum et malesuada fames ac ante ipsum primis in faucibus. Morbi vel mi ex. Etiam vitae aliquam erat, ut dapibus metus. Morbi ullamcorper maximus dignissim. Nulla semper eget ex mattis bibendum. Sed euismod est nec arcu ultrices ultricies. Integer at maximus ligula. Aliquam ultricies lacus vehicula tincidunt suscipit. Sed hendrerit odio sem, non cursus nisi euismod at. Vivamus euismod libero vitae ex efficitur, et porta massa consequat.

Vivamus mollis purus at nisi pellentesque maximus. Proin pharetra libero ac nisi molestie tincidunt. Mauris justo sem, luctus sit amet nisl non, auctor elementum quam. Aliquam erat volutpat. Vestibulum at ante sem. Integer sed placerat dolor. Sed pretium eget velit ac iaculis. Suspendisse velit urna, auctor hendrerit turpis in, sollicitudin aliquet turpis. Praesent gravida tortor odio. Nullam orci turpis, luctus ullamcorper ante non, dictum mollis mauris. Aenean maximus mauris risus, volutpat ornare massa sagittis vel. Proin finibus ornare diam at blandit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nullam imperdiet lectus vehicula sem egestas, et iaculis massa efficitur.

Fusce efficitur vitae lectus a molestie. Nam ultricies justo quis luctus cursus. Nulla ac tempor quam. Donec laoreet leo at hendrerit viverra. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nam vel ullamcorper eros. Nulla quis leo dui. Praesent sollicitudin sapien ac dignissim blandit.

Sed imperdiet erat ut suscipit porttitor. Duis maximus euismod enim. Donec eleifend lorem augue, vel vehicula elit imperdiet quis. Etiam sit amet dui sed velit lobortis consectetur quis eget nulla. In lacinia libero id iaculis pretium. Sed nec purus nec est pellentesque pulvinar in in lacus. Aenean et fringilla eros, eu dictum elit. Aliquam vel cursus dui, ac cursus ante.

Nam vulputate purus enim, eget feugiat magna eleifend sit amet. Suspendisse potenti. Aliquam ut ex neque. Curabitur sit amet mauris vel dui aliquet sagittis vitae et mauris. Vivamus laoreet nunc eu metus pulvinar tincidunt. Integer mollis, magna ut congue viverra, arcu dolor congue augue, ut rhoncus velit enim ac dui. Fusce eget condimentum est. Sed fringilla luctus ex, id mollis dui euismod nec. Aliquam et odio porttitor, laoreet erat quis, finibus velit. Proin sit amet scelerisque orci. Maecenas nulla massa, iaculis vel imperdiet a, scelerisque non lorem. Donec porttitor odio vel mauris interdum, ac venenatis mauris ultricies. Vivamus sed nisl consectetur, aliquam tortor nec, venenatis ante. Donec mollis feugiat tortor, quis tincidunt nisl suscipit elementum.

Nunc sodales fringilla lorem, eget lobortis diam fringilla fermentum. Nulla facilisi. Morbi molestie dolor ut risus tristique convallis. Ut pellentesque, eros vel volutpat finibus, ligula est molestie nunc, sit amet tempor nisi risus sit amet libero. Aliquam porta sem lacinia, laoreet orci ac, sodales nibh. Maecenas at laoreet ipsum, at faucibus nisl. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec sit amet diam tristique, dignissim lacus sed, tempor mi. Sed pulvinar lorem eget nisl fringilla, ut fringilla erat tempor. Duis pretium tincidunt leo ac volutpat. Nulla metus neque, accumsan quis rhoncus at, mollis eu diam. Aliquam venenatis eleifend velit quis convallis. Vestibulum suscipit ex tempus ultrices ullamcorper. Mauris volutpat, elit nec fringilla tincidunt, quam nulla lacinia odio, sed vehicula erat libero at leo. Proin molestie enim a dolor placerat suscipit.

Maecenas vel purus ac justo gravida feugiat at in arcu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Cras egestas hendrerit eros, at feugiat nunc pretium ut. Praesent pharetra diam tellus, eu finibus enim pulvinar a. Nulla a fermentum nunc, eget fringilla risus. Sed dapibus tempus leo nec facilisis. Nullam tincidunt est ut imperdiet tincidunt. Nam pretium suscipit laoreet. Nam pretium felis finibus sapien gravida, et lobortis dolor porta.

Quisque nec dui sit amet mauris dignissim ultrices in quis diam. Suspendisse rhoncus felis quis magna dignissim pulvinar. Suspendisse potenti. Morbi ex risus, maximus a elit eu, porta volutpat eros. Nullam porttitor sapien nisl, a maximus augue tristique sed. Suspendisse sapien orci, molestie et arcu vitae, mattis blandit sapien. Duis venenatis efficitur velit vitae suscipit. Maecenas vulputate purus id rutrum fringilla. Quisque pellentesque rutrum dignissim. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In auctor, nulla id vestibulum cursus, nulla arcu lobortis est, sit amet congue ipsum quam ac justo. Sed blandit porta sollicitudin. Quisque feugiat varius massa, eget sodales nibh porta ut. Pellentesque ut bibendum velit. Ut ultricies risus eu elit semper, sit amet lacinia risus lobortis. Aliquam at quam id lectus eleifend fermentum.

Mauris ac leo pulvinar, venenatis lorem vitae, elementum est. Duis nulla lacus, ultrices in tellus nec, mattis fringilla metus. Integer vel arcu quis tortor rhoncus accumsan. Ut vitae nibh lectus. Vestibulum viverra, lacus ut consectetur pretium, leo odio hendrerit ipsum, quis vestibulum dui purus nec arcu. Nunc cursus felis in arcu aliquet blandit eu ut massa. Donec vestibulum semper convallis. Aliquam non luctus metus. Vestibulum pretium cursus nibh, sodales varius sem semper non. Phasellus nec nisl bibendum, tincidunt eros eu, tincidunt nisl. Ut lobortis vulputate libero a varius. Fusce leo quam, blandit ut porttitor in, imperdiet a erat. Praesent est nisi, tempor ac dui nec, malesuada feugiat sem.

Phasellus lacinia neque neque, vel maximus nisi vehicula eget. Maecenas ac quam vitae eros mollis dictum eu sed dui. Vivamus quis sodales nisi. Nunc a blandit orci. Donec varius nisl sit amet lorem mattis suscipit. Curabitur faucibus blandit augue, a congue risus malesuada faucibus. Phasellus vestibulum, velit nec malesuada placerat, massa dui porta velit, eleifend dapibus velit tellus sed nibh. Integer sed mi ut neque rhoncus facilisis. Vivamus pharetra sollicitudin vulputate. Proin ac feugiat tortor. In sapien nulla, lacinia ac consectetur nec, mollis non elit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;

Etiam sagittis eros ex, ut gravida dolor ullamcorper nec. Donec sit amet pharetra tortor. Etiam mollis ultricies placerat. Suspendisse posuere rhoncus enim quis ultricies. Maecenas id dapibus augue. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris sed congue metus.

Integer rhoncus purus eget augue bibendum, quis ultrices nulla feugiat. Suspendisse mi mi, fermentum id mattis nec, pharetra a dolor. Aenean sit amet leo lacus. Sed pretium leo id felis pretium, id porttitor massa imperdiet. Donec quis sapien ultrices, fermentum turpis et, vestibulum quam. Nullam quis libero dui. Praesent venenatis leo eget venenatis facilisis. Praesent id urna a lectus viverra vestibulum. Sed placerat arcu vel nisl tristique posuere. Integer id nibh nec metus dapibus aliquam nec a urna. Nunc a congue elit. Quisque sit amet orci ac neque aliquet rhoncus eu varius sapien. Vivamus sagittis varius quam, vitae semper ligula viverra in. Maecenas rhoncus libero sed risus efficitur, pellentesque faucibus lorem feugiat. Etiam libero risus, placerat et nulla non, laoreet feugiat ipsum.

Sed egestas sem sit amet lorem sagittis imperdiet. Nam auctor nisi non bibendum mattis. Aenean vitae neque sit amet lectus fermentum vulputate id maximus erat. Suspendisse cursus dictum urna, sit amet placerat lacus varius at. Phasellus quis gravida arcu. Curabitur facilisis mattis libero. Pellentesque enim orci, fermentum quis tempor et, euismod quis ex.

Morbi interdum, ligula non dictum dignissim, leo dui sollicitudin nibh, sit amet porta justo enim eu lacus. Ut fringilla pharetra tellus nec gravida. Etiam auctor sem tincidunt est semper, vitae porta ante pulvinar. In hac habitasse platea dictumst. Aenean faucibus lorem quis metus ultricies aliquet. Sed turpis arcu, tempor vitae metus et, mattis vulputate enim. Vestibulum pharetra, massa non aliquam elementum, purus mi euismod diam, sed dictum diam sapien vel nisi.

Quisque accumsan luctus dapibus. Fusce in accumsan dolor, quis rutrum mi. Duis non molestie nunc. Proin accumsan, eros elementum tempor commodo, mi odio tempor neque, sit amet ullamcorper urna augue vel leo. Pellentesque eget mauris et turpis aliquet tincidunt in ac massa. Nulla dapibus quis justo non porta. In hac habitasse platea dictumst. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Integer aliquet odio non magna varius rutrum. In hendrerit nisi et dui vulputate commodo. Etiam efficitur purus ultricies efficitur accumsan. Cras aliquam blandit lectus, quis ultrices dolor sagittis sed. Aenean et odio sit amet turpis aliquam pellentesque nec eu ex. Integer nec laoreet quam. Fusce consequat nulla vitae justo consequat, ut lacinia ante congue. Maecenas aliquam nisi ipsum, tempor sollicitudin felis egestas et. </textarea>
                </div>
                <div class="form-check">
                    <input name="isPublic" type="checkbox" class="form-check-input" id="publicBlog">
                    <label class="form-check-label" for="exampleCheck1">Rendre l'article public</label>
                </div>
                <div class="form-group">
                    <input type="hidden" name="MAX_FILE_SIZE" value="30000" />
                    <label for="exampleFormControlFile1">Image</label>
                    <input type="file" name="image" class="form-control-file" id="exampleFormControlFile1">
                </div>
                <button type="submit" class="btn btn-primary">Valider</button>
            </form>
        </div>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="asset/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>        
    </body>
</html>