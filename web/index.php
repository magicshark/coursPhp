<?php
$db = new PDO('mysql:host=localhost;port=3306;dbname=blog', "root", "");
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$query = $db->query("Select * from article");
$articles = [];

while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
    $articles[] = $row;
}
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" href="asset/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
    </head>
    <body>
        <nav class="navbar-expand-lg navbar navbar-dark bg-primary">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="./">Accueil <span class="sr-only"></span></a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="./creerArticle.php">Rédiger un nouvel article</a>
                    </li>
                </ul>
            </div>
        </nav>
        <br />
        <div class="container-fluid">
            <div class="card-columns">
                <?Php
                foreach ($articles as $article) {
                    if ($article["isPublic"]) {
                        ?>
                        <a href="./afficherArticle.php?article=<?= $article["slug"] ?>"><div class="card bg-dark text-white">
                                <img class="card-img-top" src="./asset/upload/<?= $article["image"] ?>" alt="Card image cap">
                                <div class="card-body">
                                    <h5 class="card-title" style="color:black"><?= $article["titre"] ?></h5>
                                    <p class="card-text"><?= $article["resume"] ?></p>
                                </div>
                            </div>
                        </a>
                        <?php
                    }
                }
                ?>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="asset/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>        
</body>
</html>